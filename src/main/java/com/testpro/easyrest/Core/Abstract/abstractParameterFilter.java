package com.testpro.easyrest.Core.Abstract;

import com.testpro.easyrest.Core.Interface.ParameterFilter;
import com.testpro.easyrest.bean.ExecutionData;

/** 抽象顶层接口参数拦截器 */
public abstract class abstractParameterFilter implements ParameterFilter<ExecutionData> {}
