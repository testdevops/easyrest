package com.testpro.easyrest.Core.Interface;

public interface CacheDataSource<T> {

  T getData(String data);
}
