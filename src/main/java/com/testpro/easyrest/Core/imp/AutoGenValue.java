package com.testpro.easyrest.Core.imp;

import com.testpro.easyrest.Core.Abstract.abstractAutoGenValue;
import com.testpro.easyrest.Enum.functionEnum;
import com.testpro.easyrest.Util.StringReplaceHelper;

/** 自动生成数据数据函数类 */
public class AutoGenValue extends abstractAutoGenValue {

  @Override
  protected functionEnum getFunctionEnum(String keywords) {

    // uuid
    // random(10)

    String parameter = getParameter(keywords);

    if (parameter != null) {
      String Method = keywords.split("\\(")[0];
      for (functionEnum functionEnum : functionEnum.values()) {
        if (functionEnum.name().equals(Method)) {
          return functionEnum;
        }
      }
    } else {
      for (functionEnum functionEnum : functionEnum.values()) {
        if (functionEnum.name().equals(keywords)) {
          return functionEnum;
        }
      }
    }
    return null;
  }

  /**
   * @param keywords 关键字
   * @return 返回第一个括号中的数据字符
   */
  @Override
  protected String getParameter(String keywords) {

    return StringReplaceHelper.getKeywords(keywords, "(", ")");
  }
}
