package com.testpro.easyrest.Enum;

import cn.binarywang.tools.generator.*;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.testpro.easyrest.Core.Interface.generateByParameter;

import java.util.Date;

public enum functionEnum implements generateByParameter {
    uuid {
        @Override
        public String generateValue(String Parameter) {
            return IdUtil.simpleUUID();
        }
    },
    random {
        @Override
        public String generateValue(String Keywords) {

            if (Keywords != null) {
                return Convert.toStr(RandomUtil.randomInt(Integer.parseInt(Keywords)));
            } else {
                return Convert.toStr(RandomUtil.randomInt());
            }
        }
    }, phone {
        @Override
        public String generateValue(String Keywords) {
            return ChineseMobileNumberGenerator.getInstance().generate();
        }
    }, idCard {
        @Override
        public String generateValue(String Keywords) {
            return ChineseIDCardNumberGenerator.getInstance().generate();
        }
    },bankCard {
        @Override
        public String generateValue(String Keywords) {
            return BankCardNumberGenerator.getInstance().generate();
        }
    },name {
        @Override
        public String generateValue(String Keywords) {
            return ChineseNameGenerator.getInstance().generate();
        }
    },email{
        @Override
        public String generateValue(String Keywords) {
            return EmailAddressGenerator.getInstance().generate();
        }
    },address{
        @Override
        public String generateValue(String Parameter) {
            return ChineseAddressGenerator.getInstance().generate();
        }
    },date{
        @Override
        public String generateValue(String Parameter) {
            if (Parameter!=null){
                return DateUtil.format(new Date(),Parameter);
            }else
            return DateUtil.format(new Date(),"YYYY-MM-dd");
        }
    }
}