package com.testpro.easyrest;

import com.testpro.easyrest.Core.imp.RestAssuredImp;
import com.testpro.easyrest.bean.ExecutionData;

public class Test {

  public static void main(String[] args) {
    RestAssuredImp restAssuredImp = new RestAssuredImp();
    ExecutionData executionData = new ExecutionData();
    executionData.setCaseName("hello world");
    executionData.setUrl("http://t.weather.sojson.com/api/weather/city/101220101");
    executionData.setMethod("GET");
    executionData.setCaseDescription("case");
    restAssuredImp.execution(executionData);
  }
}
